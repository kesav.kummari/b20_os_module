#### Session Video

```
https://drive.google.com/file/d/1V3uZsnB51qHb17PcIgiLVe0EEsTMEmZl/view?usp=sharing
```

#### Creating Account with AWS 

```
STEP-1 : Open your web browser and go to the AWS Free Tier page at https://aws.amazon.com/free/.

STEP-2 : Click on the "Create a Free Account" button.

STEP-3 : Enter your email address and password in the respective fields, then click on the "Continue" button.

STEP-4 : Enter your personal information, including your name and address.

STEP-5 : Enter your payment information. AWS requires a valid credit card to verify your identity, but you won't be charged unless you exceed the limits of the free tier.

STEP-6 : Read and accept the terms and conditions by checking the box.

STEP-7 : Click on the "Create Account and Continue" button.

STEP-8 : AWS will now verify your identity by sending a verification code to the email address you provided. Enter the code in the verification form and click on the "Verify Code and Continue" button.

STEP-9 : AWS will now ask you to choose a support plan. Choose the "Basic" plan, which is free.

STEP-10 : You will now be redirected to the AWS Management Console. From here, you can start using the various AWS services that are included in the free tier.
```

#### Creating Account with GitLab 


```
Steps here 
```