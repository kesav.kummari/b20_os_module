#### Session Video

```
https://drive.google.com/file/d/12FLRkcQooQWmiRC_YsuG75XeZuksHkoW/view?usp=sharing
```
# Types of Groups

### Primary Group

- Each user account in Linux is associated with a primary group.
- The primary group is specified in the `/etc/passwd` file.
- By default, when you create a user, a group with the same name as the user is also created as the primary group.

### Secondary Groups

- Users can belong to multiple groups in addition to their primary group.
- These additional groups are known as secondary groups.
- Secondary group memberships are specified in the `/etc/group` file.

### Superuser Group (sudo or wheel)

- This is a special group that grants administrative privileges to certain users.
- Users in this group can use the `sudo` command to execute commands with superuser (root) privileges.
- The group name can vary, commonly named "sudo" or "wheel."

### System Groups

- System groups are created during the installation of various software or services.
- They are dedicated to specific system tasks or applications.
- System groups are often assigned to corresponding system users for security and access control.

### User-Created Groups

- Users can create custom groups for specific projects or collaborations.
- These groups provide an easy way to manage access to shared resources among a specific set of users.

Understanding and managing these group types allows administrators to control access and permissions efficiently in a Linux environment.

## Changing the Primary Group of a User

To change the primary group of a user, you can use the `usermod` command. Here's the basic syntax:

```bash
sudo usermod -g new_primary_group username
```
