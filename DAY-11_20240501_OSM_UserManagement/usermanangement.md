#### Session Video

```
https://drive.google.com/file/d/1Yy87KVG8jjX-u2WIV-00BIFv37BA2rih/view?usp=sharing
```
# User Administration in Linux

User Administration is the process of managing different user accounts and their respective permissions in an operating system. In Linux or Unix-based operating systems, we can create different user accounts, sort them into groups, change their set of permissions, or delete them. The terminal commands for each of the above-stated actions are discussed below.

## User Accounts

In Linux, a single user account generally belongs to a single user. The permissions for this user account determine the level of access the user will have while using the system.

### Types of User Accounts

1. **Super User (Root):**
   - The root account has all the permissions and can run all commands without any restriction.
   - The keyword `sudo` is often used to run a command as the root user.

2. **Normal User:**
   - General users of the system are assigned this account.
   - This account has restricted access to the commands and files of the system.
   - The root user can create such an account, modify its permissions, or delete it.

3. **System User:**
   - Created for a specific purpose or software (e.g., a mail account).

### User IDs (UIDs):

**User IDs are numerical identifiers assigned to each user.**

- Root user always has UID 0.
- System users often have UIDs between 1 and 999.
- Regular users usually have UIDs starting from 1000.

## Terminal Commands



```bash
# create users
sudo useradd ravi
sudo useradd Ashok

# change passwor for the user

sudo passwd ravi

# create Groups

sudo groupadd developers
sudo groupadd testers

# check the configurations files for users and group

## user
cat /etc/passwd
cat /etc/shadow

## group
cat /etc/group
cat /etc/gshadow

# create a user and add him/her to group
sudo useradd -G developer vinay

# if you want to add the existing user to the existing group
sudo usermod -a -G testers ashok

# to find the Id of the user
id ravi

# to find the what all the groups of the user he/she is part of 
groups ravi  or groups #for curent user

# to delete a user from the group
sudo  gpasswd -d ravi developers

