#### Session Video

```
https://drive.google.com/file/d/1XopV2GyvL4YOyhSBZFZjFo5GCV0eorqR/view?usp=sharing
```

## Linux File Content Commands
### 12. head Command

The head command is used to display the content of a file. It displays the first 10 lines of a file.

Syntax:

```head <file name> ``` 

### 13. tail Command

The tail command is similar to the head command. The difference between both commands is that it displays the last ten lines of the file content. It is useful for reading the error message.

Syntax:

```tail <file name>  ```
 

 
### 14. tac Command

The tac command is the reverse of cat command, as its name specified. It displays the file content in reverse order (from the last line).

Syntax:

```tac <file name>  ```
 
### 15. more command

The more command is quite similar to the cat command, as it is used to display the file content in the same way that the cat command does. The only difference between both commands is that, in case of larger files, the more command displays screenful output at a time.

> In more command, the following keys are used to scroll the page:

ENTER key: To scroll down page by line.

Space bar: To move to the next page.

b key: To move to the previous page.

/ key: To search the string.

Syntax:

```more <file name>  ```
 

 
### 16. less Command

The less command is similar to the more command. It also includes some extra features such as 'adjustment in width and height of the terminal.' Comparatively, the more command cuts the output in the width of the terminal.

Syntax:

```less <file name>  ```
 
 
## Linux User Commands
### 17. su Command

The su command provides administrative access to another user. In other words, it allows access of the Linux shell to another user.

Syntax:

```su <user name>  ```
 

### 18. id Command

The id command is used to display the user ID (UID) and group ID (GID).

Syntax:

```id```  
 
### 19. useradd Command

The useradd command is used to add or remove a user on a Linux server.

Syntax:

```useradd  username```  
  
### 20. passwd Command

The passwd command is used to create and change the password for a user.

Syntax:

```passwd <username>```  

### 21. groupadd Command

The groupadd command is used to create a user group.

Syntax:

```groupadd <group name>  ```
 