#### Session Video

```
https://drive.google.com/file/d/1g3PdBWZcyRUm2uvjaFt72jiudX65LUG7/view?usp=sharing
```
### Boot Process in Windows:

1. **Power-On Self-Test (POST):**
   - Hardware initialization and diagnostic tests.
   - Verifies essential hardware components like CPU, RAM, and storage devices.

2. **BIOS/UEFI Initialization:**
   - BIOS/UEFI firmware is loaded from ROM into memory.
   - Loads the Master Boot Record (MBR) or GUID Partition Table (GPT) from the boot device.

3. **Boot Loader Execution:**
   - MBR or GPT points to the Windows Boot Manager (BootMgr).
   - BootMgr loads and executes the Windows Boot Loader (WinLoad.exe).

4. **Windows Kernel Initialization:**
   - WinLoad.exe loads the Windows kernel (ntoskrnl.exe) into memory.
   - Kernel initializes system services, drivers, and core components.

5. **Session Manager Initialization:**
   - The Session Manager (smss.exe) is loaded, which initializes the Windows subsystem.

6. **User Mode Initialization:**
   - WinLogon.exe is loaded, presenting the user login screen.
   - After user authentication, the Shell (Explorer.exe) is launched, providing the desktop environment.

### Boot Process in Linux:

1. **Power-On Self-Test (POST):**
   - Similar to Windows, hardware initialization and diagnostic tests are performed.

2. **BIOS/UEFI Initialization:**
   - BIOS/UEFI firmware loads and executes the Master Boot Record (MBR) or GUID Partition Table (GPT) from the boot device.

3. **Boot Loader Execution:**
   - MBR or GPT points to the boot loader (GRUB, LILO, etc.).
   - The boot loader loads the Linux kernel (vmlinuz) and initial RAM disk (initrd).

4. **Linux Kernel Initialization:**
   - The kernel initializes hardware, mounts the root file system, and sets up essential data structures.
   - Kernel initializes drivers and core subsystems necessary for system operation.

5. **Init Process (Systemd):**
   - Modern Linux distributions use systemd as the init process.
   - systemd controls the boot process, manages system services, and initializes user sessions.

6. **Runlevels:**
   - Linux systems traditionally used runlevels to define the state of the system.
   - Each runlevel represents a different system configuration (e.g., single-user mode, multi-user mode).
   - Systemd replaces the concept of runlevels with target units, which provide more flexibility and granularity in system state management.

7. **User Mode Initialization:**
   - Once the init process completes, the system is ready to launch user sessions and provide a desktop environment.
   - The login manager (e.g., GDM, LightDM) presents the user login screen.
   - After user authentication, the user session is initiated, launching the desktop environment (e.g., GNOME, KDE).