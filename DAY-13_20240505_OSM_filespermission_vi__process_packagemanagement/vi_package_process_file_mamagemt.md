#### Session Video

```
https://drive.google.com/file/d/1GceeKZnnjv2UzUP_C874nH-iJS-yHroc/view?usp=sharing
```
# Types of Groups

### Primary Group

- Each user account in Linux is associated with a primary group.
- The primary group is specified in the `/etc/passwd` file.
- By default, when you create a user, a group with the same name as the user is also created as the primary group.

## Types of Groups

### Primary Group

- Each user account in Linux is associated with a primary group.
- The primary group is specified in the `/etc/passwd` file.
- By default, when you create a user, a group with the same name as the user is also created as the primary group.

### Secondary Groups

- Users can belong to multiple groups in addition to their primary group.
- These additional groups are known as secondary groups.
- Secondary group memberships are specified in the `/etc/group` file.

### Superuser Group (sudo or wheel)

- This is a special group that grants administrative privileges to certain users.
- Users in this group can use the `sudo` command to execute commands with superuser (root) privileges.
- The group name can vary, commonly named "sudo" or "wheel."

### System Groups

- System groups are created during the installation of various software or services.
- They are dedicated to specific system tasks or applications.
- System groups are often assigned to corresponding system users for security and access control.

### User-Created Groups

- Users can create custom groups for specific projects or collaborations.
- These groups provide an easy way to manage access to shared resources among a specific set of users.

Understanding and managing these group types allows administrators to control access and permissions efficiently in a Linux environment.

## Changing the Primary Group of a User

To change the primary group of a user, you can use the `usermod` command. Here's the basic syntax:

```bash
sudo usermod -g new_primary_group username
```

# Linux Overview(ownership & permissions)

Linux is a clone of UNIX, a multi-user operating system accessible by many users simultaneously. It can be employed in mainframes and servers without modifications, though security concerns arise from potential data corruption by unsolicited or malign users. To address this, Linux employs a two-level authorization system: Ownership and Permission.

## Ownership

### Linux File Ownership

Every file and directory in a Unix/Linux system is assigned three types of owners:

- **User:**
  - The creator of the file.
  - Referred to as the owner.

- **Group:**
  - A user-group containing multiple users.
  - Users in a group share the same Linux group permissions.

- **Other:**
  - Any other user with access to a file.
  - Practically, it means everybody else, often referred to as setting permissions for the world.

## Permission System on Linux

### Linux File Permissions

Each file and directory in a Unix/Linux system has three permissions for the three owners:

- **Read:**
  - Authority to open and read a file.
  - Enables listing the content of a directory.

- **Write:**
  - Authority to modify file contents.
  - Allows adding, removing, and renaming files in a directory.

- **Execute:**
  - Necessary for running executable programs.
  - If not set, you can still see/modify the program code, provided read & write permissions are set, but you cannot run it.

### Example: File Permissions in Linux

Running `ls -l` on the terminal gives a code like '-rw-rw-r--,' representing permissions for the owner, user-group, and the world.

Here, the first ‘–‘ implies that we have selected a file. Else, if it were a directory, `d` would have been shown. The characters are pretty easy to remember.

- `r` = read permission
- `w` = write permission
- `x` = execute permission
- `–` = no permission

Let us look at it this way.


- The first 'rw-' implies the owner can read and write but not execute.
- The second 'rw-' for the user-group allows reading and writing.
- The third 'r--' for the world means only reading is allowed.

## Changing Permissions Using `chmod` Command

### Absolute (Numeric) Mode

In this mode, file permissions are represented as a three-digit octal number:

| Number | Permission Type    | Symbol |
| ------ | ------------------- | ------ |
| 0      | No Permission       | —      |
| 1      | Execute             | -x     |
| 2      | Write               | -w-    |
| 3      | Execute + Write     | -wx    |
| 4      | Read                | r–    |
| 5      | Read + Execute      | r-x    |
| 6      | Read + Write        | rw-    |
| 7      | Read + Write +Execute | rwx  |

### Symbolic Mode

Uses symbols (+, -, =) to modify permissions for specific owners (u, g, o, a):

| Operator | Description                                        |
| -------- | -------------------------------------------------- |
| +        | Adds a permission to a file or directory          |
| -        | Removes the permission                             |
| =        | Sets the permission and overrides previous ones    |

The various owners are represented as:

| User Denotation | Description      |
| --------------- | ---------------- |
| u               | user/owner        |
| g               | group             |
| o               | other             |
| a               | all               |

We will not be using permissions in numbers like 755 but characters like rwx. Let’s look into an example.

## Changing Ownership and Group in Linux

For changing the ownership of a file/directory, you can use the following command:

```bash
chown user filename
```

# Package Management in Linux

## Theory

Package management is a crucial aspect of Linux systems, allowing users to efficiently install, update, and remove software packages. These packages often contain applications, libraries, and other components necessary for the functioning of the system.

Key concepts in package management include:

- **Package Repositories:** Collections of software packages. The system fetches packages from these repositories during installation.

- **Package Managers:** Tools that handle the installation, update, and removal of software packages. Common package managers include `apt` (used in Debian-based systems like Ubuntu), `yum` (used in Red Hat-based systems like CentOS), and `pacman` (used in Arch Linux).


Two ways in installing the packages in linux:

   - **System Pakage Manager Installation**
   - **Binary Installation**

## Tar Command to Compress Files in Linux

The Linux ‘tar’ stands for tape archive, which is used to create Archive and extract the Archive files. `tar` command in Linux is one of the important commands that provides archiving functionality in Linux. We can use the Linux `tar` command to create compressed or uncompressed Archive files and also maintain and modify them.

## Syntax of `tar` command in Linux

```bash
tar [options] [archive-file] [file or directory to be archived]
```
**tar:** The command itself.

**[options]:** Optional flags or settings that modify the behavior of the `tar` command.

**[archive-file]:** The name of the archive file you are creating or working with.

**[file or directory to be archived]:** The file or directory you want to include in the archive.

An Archive file is a file that is composed of one or more files along with metadata. Archive files are used to collect multiple data files together into a single file for easier portability and storage, or simply to compress files to use less storage space.

| Option | Description |
|--------|-------------|
| -c     | Creates an archive by bundling files and directories together. |
| -x     | Extracts files and directories from an existing archive. |
| -f     | Specifies the filename of the archive to be created or extracted. |
| -t     | Displays or lists the files and directories contained within an archive. |
| -u     | Archives and adds new files or directories to an existing archive. |
| -v     | Displays verbose information, providing detailed output during the archiving or extraction process. |
| -A     | Concatenates multiple archive files into a single archive. |
| -z     | Uses gzip compression when creating a tar file, resulting in a compressed archive with the ‘.tar.gz’ extension. |
| -j     | Uses bzip2 compression when creating a tar file, resulting in a compressed archive with the ‘.tar.bz2’ extension. |
| -W     | Verifies the integrity of an archive file, ensuring its contents are not corrupted. |
| -r     | Updates or adds files or directories to an already existing archive without recreating the entire archive. |


## Use Case (System Pakage Manager Installation)

Let's consider a scenario where you want to install a web server on your Linux machine for hosting a simple website.

### Step 1: Theory - Choose a Package Manager

Choose a package manager based on your Linux distribution. For example, if you're using Ubuntu, you'll use `apt`.

### Step 2: Theory - Search for the Web Server Package

Use the package manager to search for available web server packages. For `apt`, you can use:

```bash
sudo apt search web server
```
## Step 3: Theory - Install the Web Server Package

Choose a web server package (e.g., nginx), and install it using:

```bash
sudo apt install nginx
```
## Step 4: Hands-On Session

Access the web server by opening a web browser and entering [http://localhost](http://localhost). You should see the default page if the installation was successful.

## Step 5: Theory - Update and Remove Packages

Learn how to update installed packages:

```bash
sudo apt update
sudo apt upgrade
```
## Remove a Package

```bash
sudo apt remove <package_name>
```
# usecase (Binary Installation)

## Theory

### Binary Installation

Binary installations involve downloading precompiled binaries and manually installing them. This method is common when a package manager is not available or not preferred.

## Download the Binary:

```bash
wget <binary_url>
```
## Extract and Install:
```bash
 
tar -zxvf <binary_file.tar.gz>
cd <extracted_directory>
./configure
make
sudo make install
```

Package Management (apt in Ubuntu)
Package management streamlines software installation and updates.

## Search for Packages:
```bash

sudo apt search <package_name>
```
## Installing the packages
Install a Package:

```bash
sudo apt install <package_name>

```
Update Packages:
```bash
 
sudo apt update
sudo apt upgrade
```
## Use Case - Web Server Installation
Binary Installation (e.g., Nginx)
Download the Binary:

```bash
 
wget http://nginx.org/download/nginx-<version>.tar.gz

```
## Extract and Install:
```bash
 
tar -zxvf nginx-<version>.tar.gz
cd nginx-<version>
./configure
make
sudo make install

```
 ## Verify Installation:
```bash

nginx -v

```
## Start Nginx:
```bash
 
sudo nginx
```
Access the Web Server:
Open a web browser and enter ```http://localhost```. 


Cleanup
## Remove Nginx:
```bash
 
# Binary Installation Cleanup
sudo rm -rf nginx-<version>

# Package Management Cleanup
sudo apt remove nginx
```
## Conclusion
Understanding both binary installations and package management provides flexibility in handling software on your Linux system. By exploring these methods, you gain a comprehensive approach to installing and managing applications based on your preferences and system requirements.


# Package Management and Binary Installation in Linux (Yum)

## Theory


### Binary Installation

Binary installations involve downloading precompiled binaries and manually installing them. This method is common when a package manager is not available or not preferred.

- **Download the Binary:**
```bash
  wget <binary_url>
```
**Extract and Install:**

```bash
 
tar -zxvf <binary_file.tar.gz>
cd <extracted_directory>
./configure
make
sudo make install
```
# Use Case (Yum - Red Hat-based Systems)
Let's consider a scenario where you want to install a web server on your Linux machine for hosting a simple website.

## Step 1: Theory - Choose a Package Manager (Yum)
Choose yum as the package manager for Red Hat-based systems.

## Step 2: Theory - Search for the Web Server Package
Use yum to search for available web server packages. For example, you can use:

```bash
 
sudo yum search httpd
```
## Step 3: Theory - Install the Web Server Package
Choose a web server package (e.g., httpd), and install it using:

```bash
 
sudo yum install httpd
Step 4: Hands-On Session
Access the web server by opening a web browser and entering http://localhost. You should see the default page if the installation was successful.
```
## Step 5: Theory - Update and Remove Packages
Learn how to update installed packages:

```bash
 
sudo yum update
```
Learn how to remove the installed web server:

```bash
 
sudo yum remove httpd
Binary Installation (e.g., Nginx)
```
Download the Binary:

```bash
 
wget http://nginx.org/download/nginx-<version>.tar.gz
```
Extract and Install:

```bash
 
tar -zxvf nginx-<version>.tar.gz
cd nginx-<version>
./configure
make
sudo make install
```
Verify Installation:

```bash
 
nginx -v
```
Start Nginx:

```bash
 
sudo nginx
```
Access the Web Server:
Open a web browser and enter http://localhost. Verify the default Nginx page.

## Cleanup
Remove Nginx:
```bash
 
# Binary Installation Cleanup
sudo rm -rf nginx-<version>

# Package Management Cleanup
sudo yum remove nginx
```
### Conclusion
Understanding both binary installations and package management provides flexibility in handling software on your Linux system. By exploring these methods, you gain a comprehensive approach to installing and managing applications based on your preferences and system requirements.

# Process Management:

## 1. Types of Processes:

- **Foreground Processes:** These are processes that run in the foreground and interact with the user.
- **Background Processes:** These processes run in the background, allowing the user to continue working in the terminal.

## 2. Parent-Child Processes:

- **Parent Process:** Initiates the creation of another process.
- **Child Process:** Created by the parent process. It can further spawn additional child processes.

## 3. Zombie Process:

- A zombie process is a terminated child process that still has an entry in the process table.
- It exists when the child process has completed execution, but its exit status is still needed by the parent.

# Process Management Commands:
0. **$$ (current process id):**
   - Display PID of the current process
   ```bash
   echo $$
   ```
0. **$PID (current process Parent id):**
   - Display parent PID of the current process
   ```bash
   echo $$
   ```

   ```
1. **ps (Process Status):**
   - Display information about active processes.
   ```bash
   ps aux
   ```
2. **top:**

  - Dynamic real-time view of the system processes.
```bash
  top
  ```
3. **pgrep:**

  - Find the process ID of a particular process.
```bash
 
pgrep <process_name>
```
4. **pkill:**

  - Kill a process by its name.
```bash
 
pkill <process_name>
```
5. **kill:**

  - Terminate a process by its ID.
```bash
 
kill <process_ID>
```

6. **killall:**

  - Kill a process by its name.
```bash
 
killall <process_name>
```
7. **jobs:**

  - Display status and job numbers of jobs in the current session.
```bash
 
jobs
```
8. **bg:**

  - Move a job to the background.
```bash
 
bg %<job_number>
```
9. **fg:**

  - Bring a job to the foreground.
```bash
 
fg %<job_number>
```
10. **nohup:**

  - Run a command immune to hangups, with output to a non-tty.
```bash
 
nohup <command> &
```
11. **nice**:

  - Run a process with a modified scheduling priority.
```bash
 
nice -n <priority> <command>
```
12. **renice:**

  - Alter priority of running processes.
```bash
 
renice <priority> -p <process_ID>
```

# Use Case: Streamlining File Processing with Background Processes

## Scenario:
Imagine you are managing a server that processes a large number of data files regularly. The files need various types of processing, such as compression, encryption, and formatting. You want to automate this process efficiently and manage the tasks effectively, especially when dealing with large datasets.

## Solution:

**Parent Process**:

You create a script or program acting as the parent process. This process is responsible for initiating and managing child processes to handle individual files concurrently.
```bash
 
# parent_process.sh

for file in /path/to/data/*.txt; do
    # Launch child processes to process files concurrently
    ./child_process.sh "$file" &
done

# Wait for all child processes to finish
wait
```
**Child Processes:**

Child processes handle specific tasks for each file, such as compression, encryption, or any other required processing.
```bash
 
# child_process.sh

file=$1

# Perform tasks like compression, encryption, formatting, etc.
# Example: Compressing a file
gzip "$file"

echo "Processing completed for $file"
```
**Background Processing:**
By using the & symbol at the end of the child process command, you allow it to run in the background, enabling the parent process to continue its execution without waiting for each child to finish.
```bash
 
./child_process.sh "$file" &
Monitoring and Management:
```
Utilize process management commands to monitor and manage the tasks.
```bash
 
# Monitor active processes
ps aux

# Terminate a specific process if needed
kill <process_ID>
```
Outcome:

- The parent process efficiently manages the processing of multiple files concurrently.
- Background processing allows the server to continue handling other tasks without waiting for individual files to complete processing.
- Process management commands help in monitoring and controlling the execution of tasks, providing flexibility and control.

# Vi/Vim Editor:



## Introduction:
- **Vi and Vim:**
  - Vi (Visual Editor) and Vim (Vi Improved) are text editors available on most Unix-based systems.
  - Vim is an enhanced version of Vi with additional features and improvements.

## Basic Concepts:
- **Modes:**
  - **Normal Mode (or) Command mode:** Default mode for navigating and manipulating text.
  - **Insert Mode:** Used for inserting or editing text.
  - **Execute Mode:** executing the commands after the cursor.

  - **Visual Mode:** Allows selecting and manipulating text in a more visual way.

- **Switching Modes:**
  - Press `Esc` key to enter Normal Mode from Insert or Visual Mode.
  - Press `i` in Normal Mode to enter Insert Mode.
  - Press `v` in Normal Mode to enter Visual Mode.

## insert mode commands

| Command | Description                                        |
| ------- | -------------------------------------------------- |
| `i`     | Inserts text before the current cursor location   |
| `a`     | Inserts text after the current cursor location    |
| `A`     | Inserts text at the end of the current line        |
| `o`     | Creates a new line for text entry below the cursor location |
| `O`     | Creates a new line for text entry above the cursor location |


## Moving within a File
| Commands | Description |
| -------- | ----------- |
| `k`      | Moves the cursor up one line. |
| `j`      | Moves the cursor down one line. |
| `h`      | Moves the cursor to the left one-character position. |
| `l`      | Moves the cursor to the right one-character position. |
| `0`      | Positions cursor at beginning of line. |
| `$`      | Positions cursor at end of line. |
| `W`      | Positions cursor to the next word. |
| `B`      | Positions cursor to previous word. |
| `(`      | Positions cursor to beginning of current sentence. |
| `)`      | Positions cursor to beginning of next sentence. |
| `H`      | Move to top of screen. |
| `nH`     | Moves to nth line from the top of the screen. |
| `M`      | Move to middle of screen. |
| `L`      | Move to bottom of screen. |
| `nL`     | Moves to nth line from the bottom of the screen. |
| `:n`     | Positions the cursor on the line number represented by 'n'. For example, `:10` positions the cursor on line 10. |


## Navigation in Normal Mode:
- **Basic Movement:**
  - `h`: Move left
  - `j`: Move down
  - `k`: Move up
  - `l`: Move right

- **Word Movement:**
  - `w`: Move to the beginning of the next word
  - `e`: Move to the end of the next word
  - `b`: Move to the beginning of the previous word

- **Line Movement:**
  - `0`: Move to the beginning of the line
  - `$`: Move to the end of the line

- **Page Movement:**
  - `Ctrl + u`: Move up half a page
  - `Ctrl + d`: Move down half a page

## Editing in Normal Mode:
- **Basic Editing:**
  - `x`: Delete the character under the cursor
  - `dd`: Delete the entire line
  - `yy`: Copy (yank) the entire line
  - `p`: Paste the copied/yanked text after the cursor
  - `u`: Undo the last action
  - `Ctrl + r`: Redo the last undone action

## Insert Mode:
- Press `i` in Normal Mode to enter Insert Mode.
- Type and edit text as you would in any other text editor.
- Press `Esc` to return to Normal Mode.

## Visual Mode:
- Press `v` in Normal Mode to enter Visual Mode.
- Use movement keys to select text.
- Perform actions like copy, cut, or replace on the selected text.

## Use Case: Editing a Configuration File
1. **Open a File:**
   - Use `vim filename` to open a file in Vim.

2. **Navigate and Edit:**
   - Use Normal Mode to navigate to the desired location.
   - Enter Insert Mode (`i`) to make changes.

3. **Save Changes:**
   - Press `Esc` to enter Normal Mode.
   - Type `:w` and press `Enter` to save changes.

4. **Quit Vim:**
   - Type `:q` and press `Enter` to quit Vim.
   - If changes are not saved, use `:q!` to force quit.

## Advanced Features:
- **Search and Replace:**
  - `/search_term`: Search forward for a term
  - `?search_term`: Search backward for a term
  - `:s/old/new/g`: Replace all occurrences of "old" with "new" in the entire file

- **Multiple Windows:**
  - `:sp`: Split the window horizontally
  - `:vsp`: Split the window vertically
  - `Ctrl + w` + Arrow keys: Navigate between windows

- **Tabs:**
  - `:tabnew`: Open a new tab
  - `:tabnext` (or `gt` in Normal Mode): Move to the next tab
  - `:tabprev` (or `gT` in Normal Mode): Move to the previous tab


  ## Controlling Services and Daemons

### Definition

Services and daemons are background processes that run on a system, providing specific functionalities or handling system tasks.

### Key Concepts

#### Init System

- Manages the initialization of system processes.
- Common init systems include Systemd, Upstart, and SysV.

#### Service Management

- Commands like `systemctl` (Systemd) or `service` (SysV) control services.
- Examples:
  - Start a service: `sudo systemctl start <service_name>`
  - Stop a service: `sudo systemctl stop <service_name>`
  - Restart a service: `sudo systemctl restart <service_name>`
  - Enable a service on startup: `sudo systemctl enable <service_name>`
  - Disable a service on startup: `sudo systemctl disable <service_name>`

#### Daemons

- Background processes that perform system tasks.
- Often managed by init systems.
- Examples include `sshd` (SSH daemon), `httpd` (web server daemon).

## Log Management

### Logs

Logs are records of events, actions, and messages generated by the operating system, applications, and services.

### Key Concepts

#### Log Files

- Located in `/var/log`.
- Examples:
  - `/var/log/syslog`: System-wide messages.
  - `/var/log/auth.log`: Authentication-related messages.



#### Viewing Logs

Certainly! Here's a more structured and sentence-based version of your log management notes:

When exploring log files for system events, one of the primary locations to inspect is `/var/log/syslog`. This directory contains important logs that can provide insights into various activities on your system.

To efficiently check syslog entries, you can utilize commands like `head` and `tail`. For instance:

```bash
head /var/log/syslog
tail /var/log/syslog
tail -n 50 /var/log/syslog
```

These commands allow you to view the beginning, end, or a specific number of lines from the syslog file. Additionally, to continuously monitor the logs in real-time, the `tail -f` command is employed:

```bash
tail -f /var/log/syslog
```

In more recent Linux systems, another powerful tool for log management is `journalctl`. This tool provides a centralized and structured view of logs. Here are examples of how to use `journalctl`:

```bash
journalctl -u ssh
journalctl -u mysql
```

By specifying a particular unit (in this case, "ssh" or "mysql"), you can filter the logs for that specific service.

Comparing traditional syslog logs with `journalctl`, you can use `grep` with `cat` for syslog:

```bash
cat /var/log/syslog | grep mysql
```

While with `journalctl`, the equivalent command is:

```bash
journalctl -u mysql
```

Finally, for live monitoring of logs with `journalctl`, you can use the following command:

```bash
journalctl -fu mysql
```

This command provides a continuous, real-time update of the logs related to the MySQL service. Understanding these commands will empower you to effectively manage and analyze logs on your Linux system.

# MySQL Installation and Service Control Use Case

## Objective:

The goal of this use case is to guide students through the process of installing MySQL, controlling the MySQL service using `systemctl`, and checking MySQL logs using different commands.

## Steps:

### 1. MySQL Installation:

#### Step 1: Update Package List
Ensure the package list is up-to-date before installing MySQL.

```bash
sudo apt-get update
```

### 2: Install MySQL Server

Install the MySQL server package.

```bash
sudo apt-get install mysql-server
```

## 2. Controlling MySQL Service:
### Step 1: Start MySQL Service
Use systemctl to start the MySQL service.

```bash
 
sudo systemctl start mysql
```
### Step 2: Check Service Status
Verify the status of the MySQL service.

```bash
 
sudo systemctl status mysql
```
### Step 3: Restart MySQL Service
Restart the MySQL service using systemctl.

```bash
 
sudo systemctl restart mysql
```
### Step 4: Enable MySQL at Boot
Ensure MySQL starts automatically at boot.

```bash
 
sudo systemctl enable mysql
```
## 3. Checking MySQL Logs:
### Step 1: View MySQL Error Log
Check the MySQL error log for any issues.

```bash
 
sudo tail /var/log/mysql/error.log
```
### Step 2: Utilize journalctl for MySQL
On more recent systems, use journalctl for log management.

```bash
 
journalctl -u mysql
```
### Step 3: Compare with Syslog
Compare MySQL logs using grep with cat for syslog entries.

```bash
 
cat /var/log/syslog | grep mysql
```
### Step 4: Live Monitoring with journalctl
For real-time updates on MySQL logs.

```bash
 
journalctl -fu mysql
```