#### Session Video

```
https://drive.google.com/file/d/1H32ShjXpYYgoDttNWRuFcpKFArczuuyz/view?usp=sharing
```

# history of Linux
Certainly! The history of Linux is quite fascinating:

In 1991, a Finnish computer science student named Linus Torvalds began working on a personal project, creating an operating system kernel. This kernel was the essential core of an operating system that manages hardware resources and provides services for computer programs.

Linus initially developed Linux as a hobby and an educational project. He was inspired by the Unix operating system, which was powerful but not freely available. Linus aimed to create a Unix-like system that could be freely distributed and developed collaboratively.

On August 25, 1991, Linus Torvalds made an announcement on a Usenet newsgroup, presenting the first version of the Linux kernel (version 0.01). He invited others to contribute to its development, marking the birth of a collaborative, open-source project.

The project gained momentum as developers worldwide started contributing to the kernel's improvement. The Linux community expanded rapidly, and the operating system became a robust and stable platform.

Over the years, Linux has evolved into a versatile and powerful operating system used in a wide range of applications, from servers and embedded systems to desktop computers and mobile devices. Its success is attributed to the open-source model, which allows anyone to view, modify, and distribute the source code freely.

Today, Linux is a cornerstone of the open-source software movement and serves as the foundation for many operating systems, including popular distributions like Ubuntu, Fedora, CentOS, and Debian. The collaborative spirit of the Linux community continues to drive its development and innovation.

# distributions of Linux
Linux is an open-source operating system kernel, and there are numerous distributions (distros) that package the Linux kernel with other software and components to create complete, usable operating systems. Each distribution is tailored to specific needs, preferences, and use cases. Here are some notable Linux distributions:

### Ubuntu:

Known for its user-friendly interface and ease of installation.
Has both a Desktop version (for personal use) and a Server version (for server environments).
### Debian:

Emphasizes stability and free software principles.
Serves as the foundation for several other distributions, including Ubuntu.
### Fedora:

Focuses on innovation and the use of cutting-edge software.
Often seen as a testing ground for technologies that might later be included in Red Hat Enterprise Linux.
### Red Hat Enterprise Linux (RHEL):

Geared towards enterprise and business use.
Known for its stability and long-term support.
### CentOS:

A free, community-supported version of RHEL.
Shares much of the same codebase with RHEL but doesn't have the official support.
### Arch Linux:

Follows a rolling release model, providing the latest software updates.
Designed for users who want to customize their system extensively.
### openSUSE:

Known for its YaST configuration tool, making system administration easier.
Offers both openSUSE Leap (stable and reliable) and openSUSE Tumbleweed (rolling release).
### Mint:

Focused on providing a user-friendly experience for desktop users.
Comes with multimedia codecs and proprietary software out of the box.
### Gentoo:

Emphasizes performance and customization.
Users compile software from source, allowing for highly optimized installations.
### Slackware:

One of the oldest surviving Linux distributions.
Known for simplicity and adherence to Unix principles.
These are just a few examples, and there are many more specialized distributions catering to specific needs, such as Kali Linux for penetration testing, Ubuntu Studio for multimedia production, and more. Each distribution has its strengths and weaknesses, making it suitable for different use cases and preferences.


# linux 50 commands
Linux Top 50 Commands
The following are the top 50 Linux commands:

## Linux Directory Commands
### 1. pwd Command

The pwd command is used to display the location of the current working directory.

Syntax:

```pwd```  

### 2. mkdir Command

The mkdir command is used to create a new directory under any directory.

Syntax:

```mkdir <directory name>```  

### 3. rmdir Command
The rmdir command is used to delete a directory.
Syntax:

```rmdir <directory name>```  

### 4. ls Command

The ls command is used to display a list of content of a directory.

Syntax:

```ls```  

### 5. cd Command

The cd command is used to change the current directory.

Syntax:

```cd <directory name>```  
 
## Linux File commands
### 6. touch Command

The touch command is used to create empty files. We can create multiple empty files by executing it once.

Syntax:

```touch <file name>```  
```touch <file1>  <file2> ....  ```
 

### 7. cat Command

The cat command is a multi-purpose utility in the Linux system. It can be used to create a file, display content of the file, copy the content of one file to another file, and more.

Syntax:

```cat [OPTION]... [FILE]..  
To create a file, execute it as follows:

cat > <file name>  
// Enter file content  
Press "CTRL+ D" keys to save the file. To display the content of the file, execute it as follows:

cat <file name>  
 ```

 
### 8. rm Command

The rm command is used to remove a file.

Syntax:

```rm <file name>```

### 9. cp Command

The cp command is used to copy a file or directory.

Syntax:

```cp <existing file name> <new file name>```  

 
### 10. mv Command

The mv command is used to move a file or a directory form one location to another location.

Syntax:

```mv <file name> <directory path>```  

### 11. rename Command

The rename command is used to rename files. It is useful for renaming a large group of files.

Syntax:

```rename 's/old-name/new-name/' files  
For example, to convert all the text files into pdf files, execute the below command:

rename 's/\.txt$/\.pdf/' *.txt
```