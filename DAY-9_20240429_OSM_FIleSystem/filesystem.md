#### Session Video

```
https://drive.google.com/file/d/1mpiOklPthetpj-FVMjP2fCgITCREFiKU/view?usp=sharing
```
## File Systems in Windows:

### NTFS (New Technology File System):

- Developed by Microsoft.
- Offers features like file-level security, encryption, compression, and disk quotas.
- Supports large file sizes and partition sizes.
- Default file system for Windows operating systems.

### FAT32 (File Allocation Table 32):

- Older file system developed by Microsoft.
- Limited in features compared to NTFS.
- Supports smaller file sizes and partition sizes compared to NTFS.
- Compatible with various operating systems and devices.

### exFAT (Extended File Allocation Table):

- Designed by Microsoft for flash drives and other removable storage devices.
- Supports larger file sizes and partition sizes compared to FAT32.
- Offers better performance and reliability for removable storage.

## File Systems in Linux:

### ext4 (Fourth Extended Filesystem):

- Successor to ext3 and widely used in Linux distributions.
- Supports larger file sizes, larger volumes, and faster file system checking.
- Offers features like journaling, extents, and delayed allocation.

### ext3 (Third Extended Filesystem):

- Extension of the ext2 file system with journaling capability.
- Provides better reliability and faster file system recovery compared to ext2.
- Used in older Linux distributions but gradually being replaced by ext4.

### XFS (X File System):

- High-performance file system developed by Silicon Graphics International.
- Optimized for scalability and handling large files and volumes.
- Suitable for use in enterprise environments and for storage systems.

### Btrfs (B-tree File System):

- Modern file system with features like snapshots, checksums, and RAID-like functionality.
- Designed for scalability, fault tolerance, and data integrity.
- Still under active development and considered experimental in some Linux distributions.

### ZFS (Zettabyte File System):

- Advanced file system originally developed by Sun Microsystems.
- Offers features like data integrity verification, snapshots, and pooling.
- Provides built-in support for RAID and is highly scalable.


## Overview of Windows System Directories

In a Windows operating system, various directories play crucial roles in organizing system files, applications, user data, and configuration settings. Here's an overview of the most important system directories:

### 1. System32 Directory:

- **Location:** `C:\Windows\System32`
- **Description:** Contains essential system files, dynamic link libraries (DLLs), device drivers, and executables required for the proper functioning of the Windows operating system.
- **Key Components:** 
  - `kernel32.dll`: Kernel APIs.
  - `ntdll.dll`: Native API.
  - `ntoskrnl.exe`: Kernel image.
  - `services.exe`: Windows services controller.
  - `svchost.exe`: Host process for services.
  - `win32k.sys`: Kernel-mode component of the Windows subsystem.
  
### 2. Program Files Directory:

- **Location:** `C:\Program Files` (64-bit applications), `C:\Program Files (x86)` (32-bit applications on 64-bit systems)
- **Description:** Stores installed software programs.
- **Key Components:** 
  - Each installed program has its own subdirectory within the Program Files directory, containing executable files, supporting libraries, configuration files, and other resources.

### 3. Windows Directory:

- **Location:** `C:\Windows`
- **Description:** Contains core files and components of the Windows operating system.
- **Key Components:** 
  - `explorer.exe`: Windows Explorer.
  - `ntoskrnl.exe`: Kernel image.
  - `System32` subdirectory: Core system files and libraries.
  - `SysWOW64` subdirectory: Contains 32-bit system files on 64-bit systems.

### 4. Users Directory:

- **Location:** `C:\Users`
- **Description:** Contains user profile directories for each user account on the system.
- **Key Components:** 
  - Each user account has its own profile directory under the Users directory, storing user-specific settings, preferences, documents, downloads, desktop items, and other personalized data.

Understanding the purpose and contents of these system directories is essential for managing and troubleshooting Windows-based computers effectively.


## Linux System Directories

Linux systems follow a hierarchical directory structure with various directories serving specific purposes. Here's a comprehensive list of important system directories:

### 1. / (Root Directory):

- **Description:** The root directory of the file system hierarchy.
- **Key Components:** 
  - Contains all other directories and files.
  - Mount points for other file systems.

### 2. /bin (Binaries):

- **Description:** Contains essential executable binaries (commands) for system boot and operation.
- **Key Components:** 
  - Basic system commands like `ls`, `cp`, `mv`, `rm`.

### 3. /boot (Boot Files):

- **Description:** Contains boot loader files and kernel images.
- **Key Components:** 
  - Boot loader configuration files.
  - Kernel images.

### 4. /dev (Device Files):

- **Description:** Contains device files representing hardware devices.
- **Key Components:** 
  - `/null`: Represents null device (discards data written to it).
  - `/zero`: Represents zero device (returns null bytes when read).
  - `/tty`: Represents virtual consoles and serial ports.

### 5. /etc (Configuration Files):

- **Description:** Stores system-wide configuration files and settings.
- **Key Components:** 
  - `passwd`: Password file containing user account information.
  - `hosts`: Hostname-to-IP address mappings.
  - `fstab`: File system table for mounting filesystems at boot time.

### 6. /home (User Home Directories):

- **Description:** Contains home directories for regular user accounts.
- **Key Components:** 
  - Each user has a subdirectory under /home, where they store personal files, documents, settings, and configurations.

### 7. /lib (Libraries):

- **Description:** Contains shared libraries used by system binaries.
- **Key Components:** 
  - Shared object (.so) files.
  - Essential runtime libraries.

### 8. /mnt (Mount Points):

- **Description:** Temporary mount point for mounting external file systems.
- **Key Components:** 
  - Mount points for external devices like USB drives, network shares.

### 9. /opt (Optional Packages):

- **Description:** Contains optional software packages installed by the system administrator.
- **Key Components:** 
  - Third-party software installations.

### 10. /proc (Process Information):

- **Description:** Virtual file system providing information about system processes.
- **Key Components:** 
  - Process IDs (PIDs), CPU and memory usage.

### 11. /tmp (Temporary Files):

- **Description:** Stores temporary files created by system and users.
- **Key Components:** 
  - Temporary files that are automatically deleted on system reboot.

### 12. /usr (User Binaries):

- **Description:** Contains user utilities and applications.
- **Key Components:** 
  - Binaries (`/usr/bin`).
  - Libraries (`/usr/lib`).
  - Include files (`/usr/include`).

### 13. /var (Variable Files):

- **Description:** Contains variable data files, such as logs, spool files, and temporary files.
- **Key Components:** 
  - Log files (`/var/log`).
  - Spool directories (`/var/spool`).
  - Temporary files (`/var/tmp`).

Understanding the purpose and contents of these system directories is essential for managing and troubleshooting Linux-based systems effectively.