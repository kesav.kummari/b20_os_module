#### Session Video

```
https://drive.google.com/file/d/1rjDtTlXrq2y5Q4UtnLYZPWC2v0j_w96F/view?usp=sharing
```

## Linux Filter Commands
### 22. cat Command

The cat command is also used as a filter. To filter a file, it is used inside pipes.

Syntax:

```cat <fileName> | cat or tac | cat or tac |. . .   ```
 
### 23. cut Command

The cut command is used to select a specific column of a file. The '-d' option is used as a delimiter, and it can be a space (' '), a slash (/), a hyphen (-), or anything else. And, the '-f' option is used to specify a column number.

Syntax:

```cut -d(delimiter) -f(columnNumber) <fileName>  ```
  
### 24. grep Command

The grep is the most powerful and used filter in a Linux system. The 'grep' stands for "global regular expression print." It is useful for searching the content from a file. Generally, it is used with the pipe.

Syntax:

```command | grep <searchWord>  ```
 
### 25. comm Command

The 'comm' command is used to compare two files or streams. By default, it displays three columns, first displays non-matching items of the first file, second indicates the non-matching item of the second file, and the third column displays the matching items of both files.

Syntax:

```comm <file1> <file2>  ```
 
### 26. sed command

The sed command is also known as stream editor. It is used to edit files using a regular expression. It does not permanently edit files; instead, the edited content remains only on display. It does not affect the actual file.

Syntax:

```command | sed 's/<oldWord>/<newWord>/'```  
 
### 27. tee command

The tee command is quite similar to the cat command. The only difference between both filters is that it puts standard input on standard output and also write them into a file.

Syntax:

```cat <fileName> | tee <newFile> |  cat or tac |.....  ```
  
### 28. tr Command

The tr command is used to translate the file content like from lower case to upper case.

Syntax:

```command | tr <'old'> <'new'>  ```
 

 
### 29. uniq Command

The uniq command is used to form a sorted list in which every word will occur only once.

Syntax:

```command <fileName> | uniq  ```
 

 
### 30. wc Command

The wc command is used to count the lines, words, and characters in a file.

Syntax:

```wc <file name>  ```
 

 
### 31. od Command

The od command is used to display the content of a file in different s, such as hexadecimal, octal, and ASCII characters.

Syntax:
```
od -b <fileName>      // Octal format  
od -t x1 <fileName>   // Hexa decimal format  
od -c <fileName>     // ASCII character format  
 ```

 
### 32. sort Command

The sort command is used to sort files in alphabetical order.

Syntax:

```sort <file name>```  
 

 
### 33. gzip Command

The gzip command is used to truncate the file size. It is a compressing tool. It replaces the original file by the compressed file having '.gz' extension.

Syntax:

```gzip <file1> <file2> <file3>...```  
 

 
### 34. gunzip Command

The gunzip command is used to decompress a file. It is a reverse operation of gzip command.

Syntax:

```gunzip <file1> <file2> <file3>. .  ```
  
## Linux Utility Commands
### 35. find Command

The find command is used to find a particular file within a directory. It also supports various options to find a file such as byname, by type, by date, and more.

> The following symbols are used after the find command:

(.) : For current directory name

(/) : For root

Syntax:

```find . -name "*.pdf" ``` 
  
### 36. locate Command

The locate command is used to search a file by file name. It is quite similar to find command; the difference is that it is a background process. It searches the file in the database, whereas the find command searches in the file system. It is faster than the find command. To find the file with the locates command, keep your database updated.

Syntax:

```locate <file name>```  
 
### 37. date Command

The date command is used to display date, time, time zone, and more.

Syntax:

```date  ```
 

 
### 38. cal Command

The cal command is used to display the current month's calendar with the current date highlighted.

Syntax:

```cal< ``` 
 

 
### 39. sleep Command

The sleep command is used to hold the terminal by the specified amount of time. By default, it takes time in seconds.

Syntax:

```sleep <time>```  
 

 
### 40. time Command

The time command is used to display the time to execute a command.

Syntax:

```time ``` 
 

 
### 41. zcat Command

The zcat command is used to display the compressed files.

Syntax:

```zcat <file name>```  
 

 
### 42. df Command

The df command is used to display the disk space used in the file system. It displays the output as in the number of used blocks, available blocks, and the mounted directory.

Syntax:

```df  ```
 

 
### 43. mount Command

The mount command is used to connect an external device file system to the system's file system.

Syntax:

```mount -t type <device> <directory> ``` 
 

 
### 44. exit Command

Linux exit command is used to exit from the current shell. It takes a parameter as a number and exits the shell with a return of status number.

Syntax:

```exit```  
 
After pressing the ENTER key, it will exit the terminal.

### 45. clear Command

Linux clear command is used to clear the terminal screen.

Syntax:

```clear  ```

After pressing the ENTER key, it will clear the terminal screen.